import React from 'react'
import SearchInput from 'SearchInput'

export default class Home extends React.Component {
	constructor(props) {
		super(props)
	}
	render() {
		return (
			<div class="home">
				<SearchInput />
			</div>
		)
	}
}
