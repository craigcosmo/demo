import React from 'react'
import {PropTypes} from 'react'
// open source css
import 'normalize.scss'
// custom css
import 'global.scss'


export default class Page extends React.Component {
	render() {
		return (
			<div class="page">
				{React.cloneElement(this.props.children, {...this.props})}
			</div>
		)
	}
}

