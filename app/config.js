// process.env.NODE_ENV var is available during build process
const env = process.env.NODE_ENV

let port = 2020
let url = 'http://localhost:'+port+'/'
let api = 'http://localhost:'+port+'/'
let img = url+'image/'


if (env=== 'production') {
	url = 'http://chat.thatilike.com/'
	api = 'http://chat.thatilike.com/'
	img =  url+'image/'

}


export const siteUrl = url
export const sitePort = port
export default {
	siteUrl : url,
	img: img,
	ogpAPI: 'http://www.pepins.com/Backend/openDataApi.php?url=',
	registerAPI: api + 'register',
	loginAPI: api + 'login',
	getPostAPI: api + 'post',
	getTopicAPI: api + 'topic',
	postMessageAPI: api  + 'post-message',
	CreateDiscussionAPI: api + 'create-topic',

}