
import manageTranslations from 'react-intl-translations-manager'

manageTranslations({
	messagesDirectory: 'translation/defaultMessage',
	translationsDirectory: 'translation',
	languages: ['sv'] 
})

// sv is swdish locale