import ExtractTextPlugin from 'extract-text-webpack-plugin'

export default [
	{
		test: /\.js$/,
		exclude: /(node_modules|bower_components)/,
		loader: 'babel-loader'
	},
	{
		test: /\.css$/,
		// loaders: ['style', 'css?sourceMap', 'resolve-url']
		loader: ExtractTextPlugin.extract('style?sourceMap', 'css-loader?sourceMap!resolve-url-loader')
		// loader: ExtractTextPlugin.extract('style?sourceMap', 'css?modules&importLoaders=1&localIdentName=[local]_[hash:base64:3]!resolve-url')
	},
	{
		test: /\.scss$/,
		// loaders: ['style', 'css', 'resolve-url', 'sass?sourceMap']
		loader: ExtractTextPlugin.extract('css-loader?sourceMap!resolve-url-loader!sass-loader?sourceMap')
		// loader: ExtractTextPlugin.extract('style?sourceMap', 'css?modules&importLoaders=1&localIdentName=[local]_[hash:base64:3]!resolve-url!sass?sourceMap')
		// loader: ExtractTextPlugin.extract('style?sourceMap','css?modules&importLoaders=1&localIdentName=[local]_[hash:base64:3]!resolve-url!sass?sourceMap')
		// loader: ExtractTextPlugin.extract('style?sourceMap','css?modules&importLoaders=1&localIdentName=[local]_[hash:base64:3]!sass?sourceMap')
		// loaders:[
		// 	'style?sourceMap',
		// 	'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]',
		// 	'resolve-url',
		// 	'sass?sourceMap'
		// ]
	},
	{
		test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
		loader: 'file-loader?name=/font/[name].[ext]'
	},
	{
		test: /\.(woff|woff2)$/,
		loader: 'file-loader?name=/font/[name].[ext]'
	},
	{
		test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
		loader: 'file-loader?name=/font/[name].[ext]'
	},
	{
		test: /\.jpe?g$|\.gif$|\.png$|\.svg$/i,
		loaders: ['file-loader?name=/image/[name].[ext]']
	},
	{
		test: /vendor\/.+\.(jsx|js)$/,
		loader: 'imports?jQuery=jquery,$=jquery,this=>window'
	},
	{
		test: /\.json$/,
		exclude: /node_modules/,
		loader: 'json-loader'
	}

]

